import React, { Component } from 'react';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import DishDetail from './DishdetailComponent';
import Contact from './ContactComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import AboutUs from './AboutComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { addComment, fetchDishes } from '../redux/ActionCreators';
import { actions } from 'react-redux-form';

// Mapped into the props of the component
const mapStateToProps = state => {
  console.log('state: ', state);
  return {
    dishes: state.dishes, // from redux store
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders
  }
}
// to dispatch actions. 
// mapDispatchToProps recieves dispatch function as a parameter from the store after we connect it in the bottom 
const mapDispatchToProps = (dispatch) => ({
  addComment: (dishId, rating, autour, comment) => dispatch(addComment(dishId, rating, autour, comment)),
  fetchDishes: () => { dispatch(fetchDishes())},
  resetFeedbackForm: () => { dispatch(actions.reset('feedback'))}
})
// Or
/**const mapDispatchToProps = (dispatch) => {
  return {addComment: (dishId, rating, autour, comment) => dispatch(addComment(dishId, rating, autour, comment))}
} */

class Main extends Component {
  constructor(props) {
    super(props);   
  }
  componentDidMount() {
    this.props.fetchDishes();
  }
  
  render() {
    const HomePage = () => {
      return(
        <Home 
              dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
              dishesLoading={this.props.dishes.isLoading}
              dishesErrMess={this.props.dishes.errMess}
              promotion={this.props.promotions.filter((promo) => promo.featured)[0]}
              leader={this.props.leaders.filter((leader) => leader.featured)[0]}
          />
    );
    }
    const DishWithId = ({match}) => {
      return(
        //,10 - based 10- interger
            <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]}
            isLoading={this.props.dishes.isLoading}
            errMess={this.props.dishes.errMess}
            comments={this.props.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))}
            addComment={this.props.addComment}
          />
      );
    };
    return (
      <div>
        <Header />
        <Switch >
          <Route path="/home" component={HomePage} />
          {/**If we need pass props to a component, pass as a function component */}
          <Route exact path="/menu" component={() => <Menu dishes={this.props.dishes.dishes} isLoading={this.props.dishes.isLoading}/>} />
          <Route path='/menu/:dishId' component={DishWithId} />
          <Route exact path='/contactus' component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm} />} />
          <Route exact path='/aboutus' component={() => <AboutUs leaders={this.props.leaders} />} />
          {/**If path is not equal /home or /menu, redirect to home */}
          <Redirect to="/home" />
        </Switch>
        <Footer />
      </div>
    );
  }
}
// Tie the component to the redux store through the connect method
// The connect provides a wrapper container component
// mapStateToProps is called every time the store state changes
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));

// hello Ol