import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody,
    Form, FormGroup, Input, Label, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import { Loading } from './LoadingComponent';


const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isModalOpen: false}

        this.toggleModal = this.toggleModal.bind(this);
        this.submitComment = this.submitComment.bind(this);
    }
    toggleModal() {
        this.setState({isModalOpen: !this.state.isModalOpen})
    }
    submitComment(e) {    
        //alert('Rating: ' + JSON.stringify(e));
        //console.log('e: ', JSON.stringify(e));
        this.toggleModal();
        this.props.addComment(this.props.dishId, e.rating, e.username, e.comment);
    }


    render() {
        return (
            <div>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>
                        Submit Comment
                    </ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={this.submitComment}>
                            <div className='form-group'>
                                <Label htmlFor='rating'>Rating</Label>
                                <Control.select model='.rating' type="select"   name="rating" className='form-control' id='rating'>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </Control.select>
                            </div>
                            <div className='form-group'>
                                <Label htmlFor='username'>Your Name</Label>
                                <Control.text model='.username' id='username' name='username' className='form-control' placeholder='Your Name' validators={
                                        {required, minLength: minLength(3), maxLength: maxLength(15)}
                                    }/>
                                    <Errors className='text-danger'
                                    model='.username'
                                    show='touched'
                                    messages={{required: 'Required', minLength: 'Must be greater than 2 characters', maxLength: 'Must be 15 characters or less'}} />
                            </div>
                            <div className='form-group'>
                                <Label htmlFor='comment'>Comment</Label>
                                <Control.textarea model='.comment' id='comment' name='comment' rows='12' className='form-control'>
                                </Control.textarea>
                            </div>
                            <Button type="submit" value="submit" color="primary">Submit</Button>
                        </LocalForm>
                    </ModalBody>
                </Modal>
                <Button onClick={this.toggleModal} outline color='secondary'>Submit comment</Button>
            </div>
            
        )
    }
}
    function RenderDish(props) {
        //if (props.dish != null) {
            return (
                    <Card className="">
                        <CardImg width="100%" src={props.dish.image} alt={ props.dish.name } /> 
                        <CardBody>
                            <CardTitle >{ props.dish.name }</CardTitle>
                            <CardText>{ props.dish.description }</CardText>
                        </CardBody>  
                    </Card>
            )
        //}
        
    }
    function RenderComments(props) {
        if (props.comments != null) {
            return(
            <div className="">
            <h4 className="mb-2">Comments</h4>
            <ul className="list-unstyled">
                {props.comments.map((comment) => {
                    let options = { year: 'numeric', month: 'short', day: 'numeric' };
                    return <li key={comment.id} className="mb-3">
                                <div>{comment.comment}</div>
                                <div>--{comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</div>
                                {/*{new Date(comment.date).toLocaleDateString("en-US", options)}*/}
                            </li>                       
                })}
            </ul>
            <CommentForm dishId={props.dishId} addComment={props.addComment} />
            </div>
            )
        }
        else {
            return ( <div></div> )
        }
    }
    
    
    const DishDetail = (props) => {
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        //else if (props.dish != null) 
        
        //console.log('props: ', props); 
        else if (props.dish != null) {
        return (
            <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.dish.name}</h3>
                    <hr />
                </div>                
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                    <RenderComments comments={props.comments} addComment={props.addComment} dishId={props.dish.id} />
                    
                </div>
            </div>
            </div>
        );}
    }
    

export default DishDetail;